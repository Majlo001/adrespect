
/* RESIZE */
$(window).on('resize', function() {
    if ($(window).width() < 1200 && $(window).width() >= 800) {
        $('#con-right').addClass('hide');
        $('#con-left').removeClass('hide');
        $('#nav-con').addClass('hide');
    }
    else if ($(window).width() < 800) {
        $('#con-left').addClass('hide');
        $('#con-right').addClass('hide');
        $('#nav-con').removeClass('hide');
    }
    else {
        $('#navbarID').removeClass('collapse');
        $('#con-right').removeClass('hide');
        $('#con-left').addClass('hide');
        $('#nav-con').addClass('hide');
    }
    if ($(window).width() < 1200) {
        $('#navbarID').addClass('collapse');
    }
});

if ($(window).width() < 1200 && $(window).width() >= 800) {
    $('#con-right').addClass('hide');
    $('#con-left').removeClass('hide');
    $('#nav-con').addClass('hide');
}
else if ($(window).width() < 800) {
    $('#con-left').addClass('hide');
    $('#con-right').addClass('hide');
    $('#nav-con').removeClass('hide');
}
else {
    $('#navbarID').removeClass('collapse');
    $('#con-right').removeClass('hide');
    $('#con-left').addClass('hide');
    $('#nav-con').addClass('hide');
}
if ($(window).width() < 1200) {
    $('#navbarID').addClass('collapse');
}
