
/* 
Tutaj zrobione bardziej pod wizualizację.
W realnym projekcie pewnie byłoby połaczenie phpa z baza danych.
*/

const openExpand1 = document.getElementById('expand1');
const openExpand2 = document.getElementById('expand2');
const openExpand3 = document.getElementById('expand3');
const openExpand4 = document.getElementById('expand4');

const closeExpand = document.getElementById('close-expand');
const popup = document.getElementById('popup');
const popupTitle = document.getElementById('popup-title');
const overlay = document.getElementById('overlay');

let popupOpened = false;
let title = '';

openExpand1.addEventListener('click', () => {
    title = 'Komunikacja';
    ifNotOpened(title);
});
openExpand2.addEventListener('click', () => {
    title = 'Budowa wiedzy o usłudze i produktach';
    ifNotOpened(title);
});
openExpand3.addEventListener('click', () => {
    title = 'Sprzedaje';
    ifNotOpened(title);
});
openExpand4.addEventListener('click', () => {
    title = 'Świadomość marki';
    ifNotOpened(title);
});



closeExpand.addEventListener('click', () => {
    if (popupOpened == true){
        closePopup();
        popupOpened = false;
    }
});
overlay.addEventListener('click', () => {
    if (popupOpened == true){
        closePopup();
        popupOpened = false;
    }
});


function ifNotOpened(){
    if (popupOpened == false){
        popupTitle.innerHTML = title;
        openPopup();
        popupOpened = true;
    }
}
function openPopup(){
    popup.classList.add("pop-show");
    overlay.classList.add("overlay-show");
}
function closePopup(){
    popup.classList.remove("pop-show");
    overlay.classList.remove("overlay-show");
    title = '';
}
